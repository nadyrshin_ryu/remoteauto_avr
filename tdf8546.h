//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _TDF8546_H
#define _TDF8546_H

  #include "main.h"

  #if AMP_TYPE == TDF8546

    // I2C-����� � TDF8546 ����� ���� ������ � ����������� �� ������� �� ����� ���������
    // ����� 4 ��������� ��������: 0x6C, 0x6D, 0x6E, 0x6F. ���� �� ��������, ���������� ��� 4.
    #define AMP_ADR      0x6C   //00
    //#define AMP_ADR      0x6D   //01
    //#define AMP_ADR      0x6E   //10
    //#define AMP_ADR      0x6F   //11

    typedef struct 
    {
      unsigned char AmplifierStart                  :1;
      unsigned char StartupDiagnosticsEnabled       :1;
      unsigned char AC_LoadDetectionEnabled         :1;
      unsigned char Chan2_ClipInfo_STBpin           :1;
      unsigned char Chan4_ClipInfo_STBpin           :1;
      unsigned char Chan1_ClipInfo_STBpin           :1;
      unsigned char Chan3_ClipInfo_STBpin           :1;
      unsigned char ClipDetectionBelow_Vp10v        :1;
    } tIB1;

    typedef struct 
    {
      unsigned char AllChan_FastMute                :1;
      unsigned char Chan24_SoftMute                 :1;
      unsigned char Chan13_SoftMute                 :1;
      unsigned char NotUsed1                        :1;
      unsigned char NoFaultInfo_DIAGpin             :1;
      unsigned char NoTemperatureInfo_DIAGpin       :1;
      unsigned char ClipDetectionLevel              :2;
    } tIB2;

    typedef struct 
    {
      unsigned char Chan2_Disabled                  :1;
      unsigned char Chan4_Disabled                  :1;
      unsigned char Chan1_Disabled                  :1;
      unsigned char Chan3_Disabled                  :1;
      unsigned char WarningLevelTemp_135            :1;
      unsigned char Chan24_Amp_GainSelect_16db      :1;
      unsigned char Chan13_Amp_GainSelect_16db      :1;
      unsigned char NotUsed1                        :1;
    } tIB3;

    typedef struct 
    {
      unsigned char Low_VPmute_Undervoltage_7_2v    :1;
      unsigned char AC_LoadDetect_HighMeasureCurrent:1;
      unsigned char LowGainMode                     :1;
      unsigned char NotUsed1                        :1;
      unsigned char AC_LoadInfo_DBbits              :1;
      unsigned char OvervoltageWarning_16v_DIAGpin  :1;
      unsigned char SlowMute_During_Shutdown        :1;
      unsigned char SVR_CapacitorUsed               :1;
    } tIB4;

    typedef struct 
    {
      unsigned char NotUsed1                        :1;
      unsigned char NotUsed2                        :1;
      unsigned char NotUsed3                        :1;
      unsigned char NotUsed4                        :1;
      unsigned char BestEfficiency_Load_4_ohm       :1;
      unsigned char NotUsed5                        :1;
      unsigned char BestEfficiencyMode_in_2x2_chan  :1;
      unsigned char BestEfficiencyMode              :1;
    } tIB5;

    typedef union 
    {
      struct 
      {
        tIB1 IB1;
        tIB2 IB2;
        tIB3 IB3;
        tIB4 IB4;
        tIB5 IB5;
      } IBs;
      char Bytes[5];
    } tTWI_Buff;

    void CheckCmd(unsigned char *pCmdBuff, unsigned char CmdBuffSize);

  #endif

#endif