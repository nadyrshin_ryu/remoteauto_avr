//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ioavr.h>
#include "tdf8546.h"
#include "main.h"

#if AMP_TYPE == TDF8546

//==============================================================================
// ������� �������� ������� ��� TDF8546
//==============================================================================
void CheckCmd(unsigned char *pCmdBuff, unsigned char CmdBuffSize)
{
  tTWI_Buff *pBuff = (tTWI_Buff *)pCmdBuff;
  
  //if (pBuff->IBs.IB1.AmplifierStart)
  if (!(pBuff->IBs.IB2.Chan24_SoftMute))
    REM_State = 1;
  else
    REM_State = 0;
}
//==============================================================================

#endif