//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ioavr.h>
#include <inavr.h>
#include <delay.h>
#include "main.h"


unsigned char TWI_Buff[16];     // ����� ����� �� I2C
unsigned char TWI_Buff_Idx;     // ������ ������ ������ �����
unsigned char REM_State;        // ��������������� ��������� ����� REMODE
unsigned char REM_StateOld;  


//==============================================================================
// ���������� ���������� �� I2C
//==============================================================================
#pragma vector=TWI_vect
__interrupt void TWI_ISR()
{
  switch (TWSR & 0xF8)
  {
    // ������ �� I2C-slave
  case 0xa8:		// ������ ���� �����, ����� ACK, ����� ������ ������
  case 0xb8:		// ����� ����, ������� ACK
  case 0xc0:		// ����� ����, ������� NACK
    TWDR = 0xFF;
    break;
    // ������ � I2C-slave
  case 0x60:		// ������ ���� �����, ����� ACK, ����� ����� ������
    TWI_Buff_Idx = 0;
//        REMOTE_ON();
    break;
  case 0x80:		// ������ ����, ����� ACK
  case 0x88:		// ������ ����, �� ����� ACK
    TWI_Buff[TWI_Buff_Idx] = TWDR;
    if ((TWI_Buff_Idx + 1) < sizeof(TWI_Buff))
      TWI_Buff_Idx++;
    break;
  case 0xa0:		// ��� �������� � slave ���� ������� ������� STOP ��� RESTART
    CheckCmd(TWI_Buff, TWI_Buff_Idx);
    break;
    // ��������� ��������� I2C
  case 0xF8:            
  case 0x00:
    Init_Twi();
    break;
  }
        
  TWCR |= (1 << TWINT);		// ����� ����� ����������.
}
//==============================================================================


//==============================================================================
// ��������� (����)������������� I2C
//==============================================================================
void Init_Twi(void)
{
  __disable_interrupt();
  
  TWCR = (1 << TWSTO);
  TWCR = 0;
  TWAR = AMP_ADR << 1;
  TWSR = 0;
  TWCR |= (1 << TWEN) | (1 << TWIE) | (1 << TWEA);      // �������� TWI, ��������� ����������, �������� ���������.
  
  __enable_interrupt();
}
//==============================================================================


//==============================================================================
// ��������� ����������� ������ ��� � ����������� �������
//==============================================================================
void SetPowerMode(void)
{
#if SLEEP_MODE == POWER_IDLE
  SMCR = 0;                                     // ����� ����� � ������ Idle
#endif
#if SLEEP_MODE == POWER_SAVE
  SMCR = (1 << SM1) | (1 << SM0);               // ����� ����� � ������ Power-save
#endif
#if SLEEP_MODE == POWER_DOWN
  SMCR = (1 << SM1);                            // ����� ����� � ������ Power-down
#endif
#if SLEEP_MODE == POWER_ADC_NR
  SMCR = (1 << SM0);                            // ����� ����� � ������ ADC noise reduction
#endif
#if SLEEP_MODE == POWER_STANDBY
  SMCR = (1 << SM2) | (1 << SM1);               // ����� ����� � ������ Standby
#endif
#if SLEEP_MODE == POWER_STANDBY_EXT
  SMCR = (1 << SM2) | (1 << SM1) | (1 << SM0);  // ����� ����� � ������ External Standby
#endif
  
#if SLEEP_MODE != POWER_NO_SLEEP
  SMCR |= (1 << SE);                            // ��������� ���
  delay_ms(10);                                 // �������� �� ���������� ���������� ���
#endif
}
//==============================================================================


//==============================================================================
// MAIN
//==============================================================================
void main( void )
{
  REMOTE_DDR = REMOTE_Mask;
  Init_Twi();
  SetPowerMode();

  while (1)
  {
    delay_ms(10);
    
#if SLEEP_MODE != POWER_NO_SLEEP
    __sleep();          // ��������
#endif

    delay_ms(10);       // ����� �� ��������� ����� ������� (�.�. ���������� �� �� ����� ������ ������)

    // ������ �� ��������� ������ �� ����� �������� �������� ���������/���������� REMOTE
    while (REM_StateOld != REM_State)
    {
      if (REM_State)      // ����� �������� REMOTE
      {      
        delay_ms(REMOTE_ON_DELAY);
        REMOTE_ON();
        REM_StateOld = 1;
      }
      else                // ����� ��������� REMOTE
      {
        delay_ms(REMOTE_OFF_DELAY);
        REMOTE_OFF();
        REM_StateOld = 0;
      }
    }
  }
}
//==============================================================================
