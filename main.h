//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _MAIN_H
#define _MAIN_H

#include <delay.h>

// ID ����� �� ����������
#define TDA7563         1
#define TDF8546         2
// ���������� ���������
#define AMP_TYPE        TDA7563

#include "tda7563.h"
#include "tdf8546.h"


// ID ������� ���
#define POWER_NO_SLEEP          0
#define POWER_IDLE              1
#define POWER_ADC_NR            2
#define POWER_SAVE              3
#define POWER_DOWN              4
#define POWER_STANDBY           5
#define POWER_STANDBY_EXT       6


// ����� ���
#define SLEEP_MODE              POWER_DOWN      //POWER_NO_SLEEP
// �������� ���������/���������� REMOTE � ��
#define REMOTE_ON_DELAY         500
#define REMOTE_OFF_DELAY        0


// ����� �����������, ������������ ��� ����� REMOTE
#define REMOTE_Port     PORTB
#define REMOTE_DDR      DDRB
#define REMOTE_Mask     (1<<5)

// ������� ��� ���������� ������� REMOTE
#define REMOTE_ON()     REMOTE_Port = REMOTE_Mask
#define REMOTE_OFF()    REMOTE_Port &= ~REMOTE_Mask


extern unsigned char REM_State;        // ��������������� ��������� ����� REMODE


// ��������� �������
void Init_Twi(void);

#endif