//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _TDA7563_H
  #define _TDA7563_H

  #include "main.h"

  #if AMP_TYPE == TDA7563

    #define AMP_ADR      0x6C

    typedef struct 
    {
      unsigned char CD_10                           :1;
      unsigned char UnmuteRearChannels              :1;
      unsigned char UnmuteFrontChannels             :1;
      unsigned char RearChannelGain12db             :1;
      unsigned char FrontChannelGain12db            :1;
      unsigned char OffsetDetectionEnable           :1;
      unsigned char DiagnosticEnable                :1;
      unsigned char NotUsed1                        :1;
    } tIB1;

    typedef struct 
    {
      unsigned char HighEfficiencyMode_Left         :1;
      unsigned char HighEfficiencyMode_Right        :1;
      unsigned char CurrentDetectionDiagnosticEnable:1;
      unsigned char LineDriverModeDiagnostic        :1;
      unsigned char StandbyOff                      :1;
      unsigned char FastMuting                      :1;
      unsigned char NotUsed1                        :1;
      unsigned char NotUsed2                        :1;
    } tIB2;

    typedef union 
    {
      struct 
      {
        tIB1 IB1;
        tIB2 IB2;
      } IBs;
      char Bytes[2];
    } tTWI_Buff;

    void CheckCmd(unsigned char *pCmdBuff, unsigned char CmdBuffSize);

  #endif

#endif